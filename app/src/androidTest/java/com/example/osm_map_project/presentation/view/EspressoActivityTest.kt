package com.example.osm_map_project.presentation.view

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.osm_map_project.R
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(AndroidJUnit4ClassRunner::class)
class EspressoActivityTest {


    @Test
    fun input_text() {
        val scenario = ActivityScenario.launch(EspressoActivity::class.java)
        val textInput: String = "sdfsdfds"

        onView(withId(R.id.edit_text_espresso)).perform(typeText(textInput))
        onView(withId(R.id.edit_text_espresso)).check(matches(withText(textInput)))
        onView(withId(R.id.button_test_espresso)).perform(click())
    }
}