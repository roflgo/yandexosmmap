package com.example.osm_map_project.common

data class SuccessRefresh(val access_token: String, val refresh_token: String)