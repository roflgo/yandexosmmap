package com.example.osm_map_project.data.repository

import com.example.osm_map_project.data.storage.SharedRepositoryRepo
import com.example.osm_map_project.domain.repository.SharedGetData

class SharedRepositoryImpl(private val sharedGetData: SharedRepositoryRepo): SharedGetData {
    override fun saveData(): Boolean {
        return sharedGetData.saveData()
    }
}