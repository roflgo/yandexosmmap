package com.example.osm_map_project.data.storage

interface SharedRepositoryRepo {
    fun saveData() : Boolean
}