package com.example.osm_map_project.di

import com.example.osm_map_project.presentation.viewModel.YandexMapsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dataModule = module {
   viewModel{
       YandexMapsViewModel(get())
   }
}