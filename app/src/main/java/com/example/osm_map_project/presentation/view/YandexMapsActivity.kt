package com.example.osm_map_project.presentation.view

import android.Manifest
import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.osm_map_project.databinding.ActivityYandexMapsBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.yandex.mapkit.*
import com.yandex.mapkit.directions.DirectionsFactory
import com.yandex.mapkit.directions.driving.*
import com.yandex.mapkit.layers.ObjectEvent
import com.yandex.mapkit.location.LocationStatus
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.MapObjectCollection
import com.yandex.mapkit.mapview.MapView
import com.yandex.mapkit.user_location.UserLocationLayer
import com.yandex.mapkit.user_location.UserLocationObjectListener
import com.yandex.mapkit.user_location.UserLocationView
import com.yandex.runtime.Error


class YandexMapsActivity : AppCompatActivity(), UserLocationObjectListener, com.yandex.mapkit.location.LocationListener {

    private lateinit var mapView: MapView
    private lateinit var binding: ActivityYandexMapsBinding
    private lateinit var locationManager: com.yandex.mapkit.location.LocationManager
    private var mapObject: MapObjectCollection?=null
    private lateinit var currentLocation: com.yandex.mapkit.location.Location
    private lateinit var drivingRouter: DrivingRouter
    private lateinit var userLocationLayer: UserLocationLayer
    private lateinit var fused: FusedLocationProviderClient
    private var mapMarker: MapObjectCollection?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapKitFactory.setApiKey("07488be4-c2be-4756-9911-060ba936e370")
        MapKitFactory.initialize(this)
        binding = ActivityYandexMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        fused = LocationServices.getFusedLocationProviderClient(this)
        mapView = binding.maps
        mapObject = mapView.map.mapObjects.addCollection()
        mapMarker = mapView.map.mapObjects.addCollection()
        getPermission()
    }

    private fun getPermission() {
        Dexter.withContext(this).withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object: MultiplePermissionsListener{
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    getLocation()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {

                }
            }).check()
    }

    @SuppressLint("MissingPermission")
    fun getLocation() {
        val mapKit = MapKitFactory.getInstance()
        userLocationLayer = mapKit.createUserLocationLayer(mapView.mapWindow)
        userLocationLayer.isVisible = true
//        userLocationLayer.isHeadingEnabled = true  // Хз надо ли но и без него работает

//        userLocationLayer.setObjectListener(this)  // Хз надо ли но и без него работает
//        locationManager = mapKit.createLocationManager()

//        locationManager.subscribeForLocationUpdates(0.0, 0, 0.0, false, FilteringMode.OFF, locationListener) // Плавное обновление локации

        fused.lastLocation.addOnSuccessListener {location-> // Получение текущей локации
            movieCameraOnLocation(location.latitude, location.longitude)
            val loc = com.yandex.mapkit.geometry.Point(location.latitude,location.longitude)

            createRoute(loc)
        }
    }

    private fun createRoute(myLocation:com.yandex.mapkit.geometry.Point) {
        drivingRouter = DirectionsFactory.getInstance().createDrivingRouter()
        mapObject?.clear()

        val drivingSession = object : DrivingSession.DrivingRouteListener{
            override fun onDrivingRoutes(p0: MutableList<DrivingRoute>) {
                p0.forEach {
                    mapObject!!.addPolyline(it.geometry)
                }
            }

            override fun onDrivingRoutesError(p0: Error) {

            }
        }

        val navigateTo = com.yandex.mapkit.geometry.Point(56.8931, 60.6101)

        buildMapNavigator(myLocation, navigateTo, drivingRouter, drivingSession)
    }

    private fun buildMapNavigator(currentLocation: com.yandex.mapkit.geometry.Point, checkPoint: com.yandex.mapkit.geometry.Point, drivingRouter: DrivingRouter, drivingSession: DrivingSession.DrivingRouteListener) {
        val drivingOptions = DrivingOptions()
        drivingOptions.routesCount = 1
        drivingOptions.avoidTolls = true // Избегать дорожных проишествий
        val vehicleOptions = VehicleOptions()
        val requestPoints = ArrayList<RequestPoint>()

        requestPoints.add(
            RequestPoint(
                currentLocation,
                RequestPointType.WAYPOINT,
                null
            )
        )

        requestPoints.add(
            RequestPoint(
                checkPoint,
                RequestPointType.WAYPOINT,
                null
            )
        )

        drivingRouter.requestRoutes(requestPoints, drivingOptions, vehicleOptions, drivingSession)

    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onObjectAdded(p0: UserLocationView) {

    }

    override fun onObjectRemoved(p0: UserLocationView) {

    }

    override fun onObjectUpdated(p0: UserLocationView, p1: ObjectEvent) {

    }

    override fun onLocationUpdated(p0: com.yandex.mapkit.location.Location) {
        currentLocation = p0
        movieCameraOnLocation(p0.position.latitude, p0.position.longitude)
    }

    private fun setMarker(latitude: Double, longitude: Double) {
        mapMarker?.addPlacemark(com.yandex.mapkit.geometry.Point(latitude,longitude))
        mapObject?.clear()
    }

    private fun movieCameraOnLocation(latitude: Double, longitude: Double) {
        mapView.map.move(
            CameraPosition(com.yandex.mapkit.geometry.Point(latitude, longitude), 15.0f, 0.0f, 0.0f),
            Animation(Animation.Type.SMOOTH, 1f), null
        )
        setMarker(latitude, longitude)
    }

    override fun onLocationStatusUpdated(p0: LocationStatus) {

    }

    override fun onPause() {
        super.onPause()
        mapView.onStop()
        MapKitFactory.getInstance().onStop()
    }
}