package com.example.osm_map_project.presentation.view.FragmentBehavior

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.osm_map_project.R

class FirstFragment: Fragment(R.layout.first_fragment) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}